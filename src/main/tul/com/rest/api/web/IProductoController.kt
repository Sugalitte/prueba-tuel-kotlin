package com.rest.api.web

import com.rest.api.model.Producto
import org.springframework.http.ResponseEntity

interface IProductoController {

    fun listProducto(): ResponseEntity<List<Producto>>
    fun createProducto(producto: Producto): ResponseEntity<Any>
    fun updateProducto(producto: Producto): ResponseEntity<Any>
    fun deleteProducto(idPersona: Long): ResponseEntity<Any>
    fun checkOut(idCars: Long): ResponseEntity<Any>

}
