package com.rest.api.web.imp

import com.rest.api.business.producto.IProductoBusiness
import com.rest.api.exception.BusinessException
import com.rest.api.exception.NotFoundException
import com.rest.api.model.Producto
import com.rest.api.utils.Constantes
import com.rest.api.web.IProductoController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constantes.URL_BASE_PERSONAS)
class ProductoController : IProductoController {

    @Autowired
    val productoBusiness: IProductoBusiness? = null

    @GetMapping("")
    override fun listProducto(): ResponseEntity<List<Producto>> {
        return try {
            ResponseEntity(productoBusiness!!.list(), HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @PostMapping("")
    override fun createProducto(@RequestBody producto: Producto): ResponseEntity<Any> {
        return try {
            productoBusiness!!.createProducto(producto)
            ResponseEntity(HttpStatus.CREATED)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @PutMapping("/update")
    override fun updateProducto(@RequestBody producto: Producto): ResponseEntity<Any> {
        return try {
            productoBusiness!!.updateProducto(producto)
            ResponseEntity(HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @DeleteMapping("/{idProducto}")
    override fun deleteProducto(@PathVariable("idProducto") idPersona: Long): ResponseEntity<Any> {
        return try {
            productoBusiness!!.removeProducto(idPersona)
            ResponseEntity(HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        } catch (e: NotFoundException) {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PutMapping("/checkOut/{idCars}")
    override fun checkOut(@PathVariable("idCars") idCars: Long): ResponseEntity<Any> {
        return try {
            productoBusiness!!.checkOut(idCars)
            ResponseEntity(HttpStatus.OK)
        } catch (e: BusinessException) {
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }
}
