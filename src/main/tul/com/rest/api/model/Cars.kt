package com.rest.api.model

import javax.persistence.*

@Entity
@Table(name = "cars")
data class Cars(val status: String ="pending") {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_cars")
    val idCars: Long=0
}
