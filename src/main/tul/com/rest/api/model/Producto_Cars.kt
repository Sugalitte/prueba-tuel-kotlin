package com.rest.api.model

import javax.persistence.*

@Entity
@Table(name = "product_cars")
data class Producto_Cars(val idProducto: Long, val idCars: Long, val quanty: String) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val idProducto_Cars: Long = 0
}
