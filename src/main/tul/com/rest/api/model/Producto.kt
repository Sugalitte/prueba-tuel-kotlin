package com.rest.api.model

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "producto")
data class Producto(val nombre: String = "",
                    val descripcion: String = "",
                    val sku: String = ""){

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Long = 0

}

