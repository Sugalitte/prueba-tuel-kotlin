package com.rest.api.repository

import com.rest.api.model.Cars
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query

interface CarsRepository : JpaRepository<Cars, Long> {

    @Modifying
    @Query(
        "UPDATE Cars q set q.status = 'completado' where q.idCars = :idCars "
    )
    fun checkOutUpd(idCars: Long): Integer


}
