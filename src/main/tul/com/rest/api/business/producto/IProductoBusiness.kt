package com.rest.api.business.producto

import com.rest.api.model.Producto

interface IProductoBusiness {

    fun list(): List<Producto>?
    fun updateProducto(producto: Producto): Producto
    fun createProducto(producto: Producto): Producto
    fun removeProducto(idPersona: Long)
    fun checkOut(idCars: Long): Integer
}
