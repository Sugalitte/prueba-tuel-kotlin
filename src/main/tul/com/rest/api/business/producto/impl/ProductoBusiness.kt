package com.rest.api.business.producto.impl

import com.rest.api.business.producto.IProductoBusiness
import com.rest.api.exception.BusinessException
import com.rest.api.exception.NotFoundException
import com.rest.api.model.Cars
import com.rest.api.model.Producto
import com.rest.api.model.Producto_Cars
import com.rest.api.repository.CarsRepository
import com.rest.api.repository.Produc_CarRepository
import com.rest.api.repository.ProductoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class ProductoBusiness : IProductoBusiness {

    @Autowired
    val productoRepository: ProductoRepository? = null

    @Autowired
    val carsRepository: CarsRepository? = null

    @Autowired
    val proCarsRepository: Produc_CarRepository? = null

    @Throws(BusinessException::class)
    override fun list(): List<Producto>? {
        try {
            return productoRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun updateProducto(productos: Producto): Producto {
        val pro: Optional<Producto>
        try {
            pro = productoRepository!!.findById(productos.id)
        } catch (e: Exception) {
            throw  NotFoundException("No se encontró el producto con id" + productos.id)
        }

        if (pro.isPresent) {
            return productoRepository!!.save(productos)
        } else {
            throw BusinessException("No se pudo actualizar el producto")
        }
    }

    @Throws(BusinessException::class)
    override fun createProducto(producto: Producto): Producto {
        val pro: Optional<Producto>
        var cars: Optional<Cars>
        val carrito = Cars()
        try {
            pro = Optional.of(productoRepository!!.save(producto))
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (pro.isPresent) {
            cars = Optional.of(carsRepository!!.save(carrito))
        } else {
            throw BusinessException("No se pudo insertar en Cars")
        }

        if (pro.isPresent && cars.isPresent) {
            val proCars = Producto_Cars(pro.get().id, cars.get().idCars, "disponible")
            proCarsRepository!!.save(proCars)
        } else {
            throw BusinessException("No se pudo insertar en Produc_Cars")
        }

        return pro.get();

    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun removeProducto(idProducto: Long) {
        val op: Optional<Producto>

        try {
            op = productoRepository!!.findById(idProducto)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("No se encuentra el producto con id=$idProducto")
        } else {
            try {
                productoRepository!!.deleteById(idProducto)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun checkOut(idCars: Long): Integer {
        val cars: Optional<Cars>
        try {
            cars = carsRepository!!.findById(idCars)
        } catch (e: Exception) {
            throw  NotFoundException("No se encontró el Cars con id $idCars")
        }

        if (cars.isPresent) {
            return carsRepository!!.checkOutUpd(idCars)
        } else {
            throw BusinessException("No se pudo actualizar el producto")
        }
    }

}
